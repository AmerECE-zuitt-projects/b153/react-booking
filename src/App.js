import React from "react";
import AppNavbar from "./components/AppNavbar";
// import Banner from "./components/Banner";
// import Highlights from "./components/Highlights";
import { Container } from "react-bootstrap";
import Home from "./pages/Home";
import Courses from "./pages/Courses";

import "./App.css";

function App() {
  return (
    <React.Fragment>
      <AppNavbar />
      <Container>
        <Home />
        <Courses />
      </Container>
    </React.Fragment>
  );
}

export default App;
