import { Row, Col, Card } from "react-bootstrap";

const Highlights = () => {
  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Title>
            <h2>Learn From Home</h2>
          </Card.Title>
          <Card.Text>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis,
            pariatur eaque praesentium expedita molestiae incidunt doloribus
            quia voluptates provident aliquam natus quos quo sit, corrupti
            autem. Nemo aliquam natus ad.
          </Card.Text>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Title>
            <h2>Study Now, Pay Later</h2>
          </Card.Title>
          <Card.Text>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis,
            pariatur eaque praesentium expedita molestiae incidunt doloribus
            quia voluptates provident aliquam natus quos quo sit, corrupti
            autem. Nemo aliquam natus ad.
          </Card.Text>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3">
          <Card.Title>
            <h2>Be part of our community</h2>
          </Card.Title>
          <Card.Text>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Facilis,
            pariatur eaque praesentium expedita molestiae incidunt doloribus
            quia voluptates provident aliquam natus quos quo sit, corrupti
            autem. Nemo aliquam natus ad.
          </Card.Text>
        </Card>
      </Col>
    </Row>
  );
};

export default Highlights;
