import { Row, Col, Button } from "react-bootstrap";

const Banner = () => {
  return (
    <Row className="p-5">
      <Col>
        <h1>Zuitt Coding Bootcamp</h1>
        <p>Opportunities for everyone, everywhere</p>
        <Button variant="primary">Enroll Now!</Button>
      </Col>
    </Row>
  );
};

export default Banner;
