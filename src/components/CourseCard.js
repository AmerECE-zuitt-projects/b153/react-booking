import { Card, Button } from "react-bootstrap";
import { useState } from "react";

const CourseCard = ({ courseProp }) => {
  const { id, name, description, price, onOffer } = courseProp;

  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);

  const enroll = () => {
    if (seats > 0) {
      setCount(count + 1);
      setSeats(seats - 1);
    } else {
      alert("No more available seats");
    }
  };
  return (
    <Card className="my-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Card.Text>Available seats: {seats}</Card.Text>
        <Card.Text>Enroll: {count}</Card.Text>
        <Button variant="primary" onClick={enroll}>
          Enroll
        </Button>
      </Card.Body>
    </Card>
  );
};

export default CourseCard;
