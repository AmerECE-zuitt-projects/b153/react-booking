import courseData from "./../data/Courses";
import CourseCard from "../components/CourseCard";
import React from "react";

const Courses = () => {
  // console.log(courseData);

  const courses = courseData.map((element) => {
    // console.log(element);
    return <CourseCard key={element.id} courseProp={element} />;
  });
  return <React.Fragment>{courses}</React.Fragment>;
};

export default Courses;
