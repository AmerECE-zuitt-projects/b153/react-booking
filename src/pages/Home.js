import React from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
// import CourseCard from "../components/CourseCard";

const Home = () => {
  return (
    <React.Fragment>
      <Banner />
      <Highlights />
    </React.Fragment>
  );
};

export default Home;
